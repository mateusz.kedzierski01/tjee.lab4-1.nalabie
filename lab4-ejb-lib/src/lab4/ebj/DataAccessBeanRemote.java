package lab4.ebj;

import java.util.Date;
import javax.ejb.Remote;

@Remote
public interface DataAccessBeanRemote {
    String getOstatnieDane();
    void setOstatnieDane(String dane, Date data);
    void increaseLicznik();
    int getLicznik();
    
}
