package lab4.ebj;
import java.util.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Stateless
@LocalBean
@WebService(serviceName = "DataAccessSOAP")
public class DataAccessBean {
    
    private String OstatnieDane;
    private Date OstatniDostep;
    private int licznik;
    
    public String getOstatnieDane() {
        return OstatniDostep + " | " + OstatnieDane;
    }
    
    public void setOstatnieDane(String dane, Date data) {
        this.OstatnieDane = dane;
        this.OstatniDostep = data;
    }
    
    @WebMethod(operationName = "DostepKlienta")
    public String DostepKlienta(@WebParam(name = "dane") String dane) {
        String odpowiedz = "Ostatnie dane: " + this.getOstatnieDane();
        this.setOstatnieDane(dane, new java.util.Date());
        return odpowiedz;
    }
    
    @WebMethod(operationName = "Odwiedziny")
    public String Odwiedziny() {
        this.licznik++;
        String odpowiedz = "Strona odwiedzona: " + this.licznik + " razy.";
        return odpowiedz;
    }
}