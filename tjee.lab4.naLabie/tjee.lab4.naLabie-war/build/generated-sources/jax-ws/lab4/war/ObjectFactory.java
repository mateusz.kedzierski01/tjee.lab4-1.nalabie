
package lab4.war;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the lab4.war package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DostepKlienta_QNAME = new QName("http://ebj.lab4/", "DostepKlienta");
    private final static QName _DostepKlientaResponse_QNAME = new QName("http://ebj.lab4/", "DostepKlientaResponse");
    private final static QName _Odwiedziny_QNAME = new QName("http://ebj.lab4/", "Odwiedziny");
    private final static QName _OdwiedzinyResponse_QNAME = new QName("http://ebj.lab4/", "OdwiedzinyResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: lab4.war
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DostepKlienta }
     * 
     */
    public DostepKlienta createDostepKlienta() {
        return new DostepKlienta();
    }

    /**
     * Create an instance of {@link DostepKlientaResponse }
     * 
     */
    public DostepKlientaResponse createDostepKlientaResponse() {
        return new DostepKlientaResponse();
    }

    /**
     * Create an instance of {@link Odwiedziny }
     * 
     */
    public Odwiedziny createOdwiedziny() {
        return new Odwiedziny();
    }

    /**
     * Create an instance of {@link OdwiedzinyResponse }
     * 
     */
    public OdwiedzinyResponse createOdwiedzinyResponse() {
        return new OdwiedzinyResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DostepKlienta }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DostepKlienta }{@code >}
     */
    @XmlElementDecl(namespace = "http://ebj.lab4/", name = "DostepKlienta")
    public JAXBElement<DostepKlienta> createDostepKlienta(DostepKlienta value) {
        return new JAXBElement<DostepKlienta>(_DostepKlienta_QNAME, DostepKlienta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DostepKlientaResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DostepKlientaResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://ebj.lab4/", name = "DostepKlientaResponse")
    public JAXBElement<DostepKlientaResponse> createDostepKlientaResponse(DostepKlientaResponse value) {
        return new JAXBElement<DostepKlientaResponse>(_DostepKlientaResponse_QNAME, DostepKlientaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Odwiedziny }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Odwiedziny }{@code >}
     */
    @XmlElementDecl(namespace = "http://ebj.lab4/", name = "Odwiedziny")
    public JAXBElement<Odwiedziny> createOdwiedziny(Odwiedziny value) {
        return new JAXBElement<Odwiedziny>(_Odwiedziny_QNAME, Odwiedziny.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OdwiedzinyResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link OdwiedzinyResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://ebj.lab4/", name = "OdwiedzinyResponse")
    public JAXBElement<OdwiedzinyResponse> createOdwiedzinyResponse(OdwiedzinyResponse value) {
        return new JAXBElement<OdwiedzinyResponse>(_OdwiedzinyResponse_QNAME, OdwiedzinyResponse.class, null, value);
    }

}
